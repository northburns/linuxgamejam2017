# insert game title here

This is Northburns'PLAYER_ID entry for the [Linux Game Jam 2017](https://itch.io/jam/linux-jam-2017).

You can either run the game straight from source, or go to Itch.io and download a distributable.

## Running the game from source

You must have JDK >=8 installed. Then run the following

```bash
./gradlew run
```

## Itch.io page (with distributables)

__TODO:__ An itch.io page for the game. Also, upload distributable files, preferable bundled with the JRE, so that the user doesn't have to have a JRE or anything like that installed.

# About the game

__TODO:__ Game description. Maybe some nice pics?

# About the source

Work begun with the LibGDX setup app, and then adding all a good deal of LibGDX extensions, Kotlin support, and KTX. Extraneous libraries should be removed before final submission.

# Linux Game Jam 2017

__TODO:__ _Really_ quickly what the jam is, and so on and so on.

The most profilic rules are copied here:

* Your game'PLAYER_ID art assets and programming should be made within said time frame.
  * Using Kenneys Game Assets as place holder, but I'll have to allocate time to do the actual assets as well
* Your game must have a Linux build.
  * At least a JAR file will be provided, but an executable with a bundled JRE would be nice
* You get exactly one bonus point if your game is open source.
  * Nice!




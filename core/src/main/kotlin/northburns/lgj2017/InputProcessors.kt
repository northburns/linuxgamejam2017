package northburns.lgj2017

import com.badlogic.gdx.InputAdapter
import com.badlogic.gdx.InputProcessor
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.utils.ObjectIntMap
import com.badlogic.gdx.utils.IntMap
import com.badlogic.gdx.utils.ArrayMap
import com.badlogic.gdx.utils.Predicate
import org.omg.CORBA.UNKNOWN


class UiInputProcessor : InputAdapter()

class GameInputProcessor<T> : InputAdapter() {
    private val NO_KEYCODE = Input.Keys.UNKNOWN
    private val onDown: ArrayMap<T, () -> Boolean> = ArrayMap()
    private val keycodeToKey: IntMap<T> = IntMap()
    private val keyToKeycode: ObjectIntMap<T> = ObjectIntMap()

    fun setInputKey(key: T, keycode: Int) {
        keycodeToKey.put(keycode, key)
        keyToKeycode.put(key, keycode)
    }

    fun onDownU(key: T, operation: (T) -> Unit) {
        onDown.put(key) {
            operation(key)
            true
        }
    }
    fun onDown(key: T, operation: (T) -> Boolean) {
        onDown.put(key) {
            operation(key)
        }
    }

    fun isDown(key: T): Boolean {
        if (keyToKeycode.containsKey(key)) {
            val keycode = keyToKeycode.get(key, NO_KEYCODE)
            return Gdx.input.isKeyPressed(keycode)
        }
        return false
    }

    override fun keyDown(keycode: Int): Boolean {
        if (keycodeToKey.containsKey(keycode)) {
            val key = keycodeToKey.get(keycode)
            if (onDown.containsKey(key))
                return onDown.get(key)()
        }
        return false
    }

}

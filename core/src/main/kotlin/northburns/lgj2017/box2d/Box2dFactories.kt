package northburns.lgj2017.box2d

import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.*
import java.util.stream.IntStream

fun createUnitBalls(count: Int, world: World, x: Float = 0f, y: Float = 0f): List<Body> {
    val circle = CircleShape()
    circle.radius = 0.5f

    val fixtureDef = FixtureDef()
    fixtureDef.shape = circle
    fixtureDef.density = 0.5f
    fixtureDef.friction = 0.4f
    fixtureDef.restitution = 0.6f // Make it bounce a little bit

    val bodyDef = BodyDef()
    bodyDef.type = BodyDef.BodyType.DynamicBody
    bodyDef.position.set(x, y)

    val bodies = (1..count).map {
        val body = world.createBody(bodyDef)
        body.createFixture(fixtureDef)
        body
    }

    circle.dispose()

    return bodies
}

/**
 * Area is rectangular and centered at (0,0)
 */
fun createWallsForArea(area: Vector2, world: World) {

    val wallHalfThickness = 10f

    val areaHalfWidth = area.x / 2f
    val areaHalfHeight = area.y / 2f

    val groundBodyDef = BodyDef()
    val groundBox = PolygonShape()

    val f: (Vector2, Vector2) -> Unit = { pos, halfSize ->
        groundBodyDef.position.set(pos)
        groundBox.setAsBox(halfSize.x, halfSize.y)

        world.createBody(groundBodyDef).createFixture(groundBox, 0f)
    }

    val pos = Vector2()
    val halfSize = Vector2()

    f(pos.set(0f, areaHalfHeight + wallHalfThickness), halfSize.set(areaHalfWidth, wallHalfThickness))
    f(pos.set(0f, -(areaHalfHeight + wallHalfThickness)), halfSize.set(areaHalfWidth, wallHalfThickness))
    f(pos.set(areaHalfWidth + wallHalfThickness, 0f), halfSize.set(wallHalfThickness, areaHalfHeight))
    f(pos.set(-(areaHalfWidth + wallHalfThickness), 0f), halfSize.set(wallHalfThickness, areaHalfHeight))

    groundBox.dispose()

}
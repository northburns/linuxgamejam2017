package northburns.lgj2017

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.TextureRegion
import ktx.assets.Assets
import ktx.assets.asset
import ktx.assets.load

enum class Images(
        private val fileName: String,
        private val dirName: String = "imagesPlaceholder") {
    player("pieceGreen_border08"),
    enemy("pieceBlue_border09"),
    ground("slice31_31");

    val path = "$dirName/$fileName.png"
    fun load() = load<Texture>(path)
    operator fun invoke() = asset<Texture>(path)
}


fun loadAll() {
    Images.values().forEach { it.load().finishLoading() }
    // and others
}

fun disposeAll() {
    Assets.manager.dispose()
}
package northburns.lgj2017

import com.badlogic.gdx.Game
import com.badlogic.gdx.Input
import com.badlogic.gdx.Input.Keys.RIGHT
import com.badlogic.gdx.math.Vector2



val Box2dUpdateInterval = 1f / 30f

/*
 * Flags
 */
val Box2dDebugDraw = true

/*
 * Game "settings"
 */
val GameAreaSize = Vector2(15f, 15f)
val DefaultViewPortSize = Vector2(10f, 10f)

/*
 * Physics constants
 */
val PlayerMoveImpulse = 1.5f
val PlayerLinearDamping = 5f
val PlayerAngularDamping = 5f


/**
 * Input keys are hard coded, that's why this is here :)
 */
enum class GameInput(val key: Int) {
    UP(Input.Keys.UP),
    RIGHT(Input.Keys.RIGHT),
    DOWN(Input.Keys.DOWN),
    LEFT(Input.Keys.LEFT),

    SPOOK(Input.Keys.X),

    ZOOM_OUT(Input.Keys.U),
    ZOOM_IN(Input.Keys.O),
    ;
    companion object {
        fun register(processor: GameInputProcessor<GameInput>) {
            values().forEach {
                processor.setInputKey(it, it.key)
            }
        }
    }
}
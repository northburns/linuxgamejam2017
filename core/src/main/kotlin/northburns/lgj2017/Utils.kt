package northburns.lgj2017

import com.badlogic.ashley.utils.ImmutableArray

/*
 * If you _know_ that there is exactly one element in an ImmutableArray, use this.
 */
fun <T> ImmutableArray<T>.only(): T {
    if(count() != 1)
        throw IllegalStateException("ImmutableArray didn't contain exactly 1 element, even though assumed so.")
    return first()
}


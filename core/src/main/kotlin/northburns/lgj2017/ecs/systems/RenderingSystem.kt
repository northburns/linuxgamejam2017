package northburns.lgj2017.ecs.systems

import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.EntitySystem
import com.badlogic.ashley.core.Family
import com.badlogic.ashley.systems.SortedIteratingSystem
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Vector3
import northburns.lgj2017.Box2dDebugDraw
import northburns.lgj2017.ecs.*
import java.util.*
import java.util.Comparator.comparingInt

private val F = Family.all(FixedSprite::class.java, Positioned::class.java).get()

class RenderingSystem(
        private val camera: Camera,
        private val batch: Batch,
        private val box2dDebugRenderer: Box2dDebugRenderer,
        priority: Int = 0
) : SortedIteratingSystem(
        F,
        comparingInt { entity -> ComponentMapper_Positioned.get(entity).z },
        priority) {

    private val v = Vector3()

    override fun update(deltaTime: Float) {
        camera.update()

        // Will there be several viewports? Maaayyybeeeee.
        // viewport.apply()


        batch.begin()
        batch.projectionMatrix = camera.combined

        super.update(deltaTime)

        batch.end()

        if (Box2dDebugDraw)
            box2dDebugRenderer.render(camera)
    }


    override fun processEntity(entity: Entity?, deltaTime: Float) {
        val texture = ComponentMapper_FixedSprite.get(entity).image
        val pos = ComponentMapper_Positioned.get(entity)

        val translateX = pos.halfWidth
        val translateY = pos.halfHeight

        println(translateX)

        val worldWidth = pos.width
        val worldHeight = pos.height

        batch.draw(texture, pos.x -translateX , pos.y -translateY, translateX, translateY, worldWidth, worldHeight, 1f, 1f,
                pos.angleRad * MathUtils.radiansToDegrees);

    }

}



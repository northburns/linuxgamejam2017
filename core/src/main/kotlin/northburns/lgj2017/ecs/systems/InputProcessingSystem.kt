package northburns.lgj2017.ecs.systems

import com.badlogic.ashley.core.EntitySystem
import com.badlogic.ashley.core.Family
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.math.Vector2
import northburns.lgj2017.*
import northburns.lgj2017.ecs.*

class InputProcessingSystem(
        private val input: GameInputProcessor<GameInput>,
        priority: Int = 0
) : EntitySystem(priority) {

    val f = Family.all(PlayerControlled::class.java, PhysicsObject::class.java).get()

    init {
        input.onDownU(GameInput.SPOOK) {
            println("Boo!")
        }
    }

    private val v = Vector2()

    override fun update(deltaTime: Float) {
        val player = engine.getEntitiesFor(f).only()

        val control = ComponentMapper_PlayerControlled.get(player)
        val body = ComponentMapper_PhysicsObject.get(player).obj

        body.linearDamping = PlayerLinearDamping
        body.angularDamping = PlayerAngularDamping

        v.set(0f, 0f)

        if (control.moveUp)
            v.add(0f, PlayerMoveImpulse)
        if (control.moveDown)
            v.add(0f, -PlayerMoveImpulse)
        if (control.moveLeft)
            v.add(-PlayerMoveImpulse, 0f)
        if (control.moveRight)
            v.add(PlayerMoveImpulse, 0f)

        body.applyLinearImpulse(v, body.position, true)

        if (control.wantsSpook) {
//            position.x += 10f
        }

    }

}


package northburns.lgj2017.ecs.systems

import com.badlogic.ashley.core.EntitySystem
import com.badlogic.ashley.core.Family
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.math.MathUtils
import northburns.lgj2017.GameInput
import northburns.lgj2017.GameInputProcessor
import northburns.lgj2017.ecs.ComponentMapper_PlayerControlled
import northburns.lgj2017.ecs.ComponentMapper_Positioned
import northburns.lgj2017.ecs.PlayerControlled
import northburns.lgj2017.ecs.Positioned
import northburns.lgj2017.only

class CameraSystem(
        private val camera: OrthographicCamera,
        priority: Int = 0
) : EntitySystem(priority) {

    val FAMILY_PLAYER = Family.all(PlayerControlled::class.java, Positioned::class.java).get()

    override fun update(deltaTime: Float) {
        val player = engine.getEntitiesFor(FAMILY_PLAYER).only()

        val control = ComponentMapper_PlayerControlled.get(player)
        val position = ComponentMapper_Positioned.get(player)

        // TODO: interlopate to that position
        camera.position.set(position.x, position.y, 0f)

        val zoomSpeed = 0.2f * deltaTime

        if(control.zoomIn)
            camera.zoom += zoomSpeed
        if(control.zoomOut)
            camera.zoom -= zoomSpeed

        // TODO: Clamp to some good-ish values or something :)
        //camera.zoom = MathUtils.clamp(camera.zoom, 1.0f ,1.0f)




    }

}


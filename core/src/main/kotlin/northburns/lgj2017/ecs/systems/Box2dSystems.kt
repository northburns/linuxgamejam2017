package northburns.lgj2017.ecs.systems

import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.EntitySystem
import com.badlogic.ashley.core.Family
import com.badlogic.ashley.systems.IntervalSystem
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.Box2D
import northburns.lgj2017.Box2dUpdateInterval
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer
import com.badlogic.gdx.physics.box2d.World
import northburns.lgj2017.ecs.ComponentMapper_PhysicsObject
import northburns.lgj2017.ecs.ComponentMapper_Positioned
import northburns.lgj2017.ecs.PhysicsObject
import northburns.lgj2017.ecs.Positioned


class Box2dUpdateSystem(
        private val world: World,
        priority: Int = 0
) : IntervalSystem(
        Box2dUpdateInterval,
        priority
) {

    override fun updateInterval() {
        world.step(Box2dUpdateInterval, 6, 2);
    }

}

private val F_BODY_POSITIONED = Family.all(PhysicsObject::class.java, Positioned::class.java).get()

class Box2dBodyToPositionedSystem(
    priority: Int
) : IteratingSystem(
        F_BODY_POSITIONED,
        priority
) {

    val v = Vector2()

    override fun processEntity(entity: Entity, deltaTime: Float) {
        val pos = ComponentMapper_Positioned[entity]
        val obj = ComponentMapper_PhysicsObject[entity].obj

        val worldPoint = obj.getWorldPoint(v.set(0f, 0f))

        pos.x = worldPoint.x
        pos.y = worldPoint.y

        pos.angleRad = obj.angle

        println("${pos.x}, ${pos.y} @ ${pos.angleRad}")
    }

}


class Box2dDebugRenderer (
        val debugRenderer: Box2DDebugRenderer,
        private val world: World
) {

    private val drawBodies = true
    private val drawJoints = true
    private val drawAABBs = false
    private val drawInactiveBodies = true
    private val drawVelocities = true
    private val drawContacts = true

    init {
        debugRenderer.isDrawBodies = drawBodies
        debugRenderer.isDrawJoints = drawJoints
        debugRenderer.isDrawAABBs = drawAABBs
        debugRenderer.isDrawInactiveBodies = drawInactiveBodies
        debugRenderer.isDrawVelocities = drawVelocities
        debugRenderer.isDrawContacts = drawContacts
    }

    fun render(camera: Camera) {
        debugRenderer.render(world,camera.combined)
    }
}
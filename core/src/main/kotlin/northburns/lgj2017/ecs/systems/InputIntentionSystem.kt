package northburns.lgj2017.ecs.systems

import com.badlogic.ashley.core.EntitySystem
import com.badlogic.ashley.core.Family
import com.badlogic.gdx.Gdx
import northburns.lgj2017.GameInput
import northburns.lgj2017.GameInputProcessor
import northburns.lgj2017.ecs.ComponentMapper_PlayerControlled
import northburns.lgj2017.ecs.PlayerControlled
import northburns.lgj2017.ecs.Positioned
import northburns.lgj2017.only

class InputIntentionSystem(
        private val input: GameInputProcessor<GameInput>,
        priority: Int = 0
) : EntitySystem(priority) {

    val f: Family = Family.all(PlayerControlled::class.java).get()

    var spookCount = 0

    init {
        input.onDownU(GameInput.SPOOK) {
            spookCount++
        }
    }

    override fun update(deltaTime: Float) {
        val player = engine.getEntitiesFor(f).only()
        ComponentMapper_PlayerControlled.get(player).update()
    }

    val update: PlayerControlled.() -> Unit = {
        moveUp = input.isDown(GameInput.UP)
        moveRight = input.isDown(GameInput.RIGHT)
        moveDown = input.isDown(GameInput.DOWN)
        moveLeft = input.isDown(GameInput.LEFT)

        zoomIn = input.isDown(GameInput.ZOOM_IN)
        zoomOut = input.isDown(GameInput.ZOOM_OUT)

        if (spookCount > 0) {
            wantsSpook = true
            spookCount = 0
        }
    }


}


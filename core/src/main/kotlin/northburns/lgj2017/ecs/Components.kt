package northburns.lgj2017.ecs

import com.badlogic.ashley.core.Component
import com.badlogic.ashley.core.ComponentMapper
import com.badlogic.ashley.core.Family
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.physics.box2d.Body

data class PlayerControlled(
        val controllerId: String
) : Component {
    companion object {
        val CM = ComponentMapper.getFor(PlayerControlled::class.java)
    }

    var moveUp: Boolean = false
    var moveRight: Boolean = false
    var moveDown: Boolean = false
    var moveLeft: Boolean = false

    var wantsSpook: Boolean = false
    var zoomIn: Boolean = false
    var zoomOut: Boolean = false

}

data class FixedSprite(
        val image: TextureRegion
) : Component {
    constructor(texture: Texture) : this(TextureRegion(texture))
}


data class Positioned(
        var x: Float = 0f,
        var y: Float = 0f,
        /**
         * 0 is up, increases anti-clockwise, right?
         */
        var angleRad: Float = 0f,
        var z: Int = 0,
        val width: Float = 1f,
        val height: Float = 1f
) : Component {
    val halfWidth = width / 2f
    val halfHeight = height / 2f

}

data class PhysicsObject(
        var obj: Body
) : Component

/*
 * Component mappers
 */

val ComponentMapper_PlayerControlled = ComponentMapper.getFor(PlayerControlled::class.java)
val ComponentMapper_FixedSprite = ComponentMapper.getFor(FixedSprite::class.java)
val ComponentMapper_Positioned = ComponentMapper.getFor(Positioned::class.java)
val ComponentMapper_PhysicsObject = ComponentMapper.getFor(PhysicsObject::class.java)

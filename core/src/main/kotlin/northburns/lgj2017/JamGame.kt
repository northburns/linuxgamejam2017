package northburns.lgj2017

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.*
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.utils.viewport.FitViewport
import ktx.app.KotlinApplication
import ktx.assets.Assets
import northburns.lgj2017.ecs.FixedSprite
import northburns.lgj2017.ecs.PlayerControlled
import northburns.lgj2017.ecs.Positioned
import com.badlogic.gdx.utils.viewport.Viewport
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer
import com.badlogic.gdx.physics.box2d.World
import ktx.app.clearScreen
import com.badlogic.gdx.InputMultiplexer
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.scenes.scene2d.ui.Value
import com.badlogic.gdx.utils.viewport.ScreenViewport
import northburns.lgj2017.box2d.createUnitBalls
import northburns.lgj2017.box2d.createWallsForArea
import northburns.lgj2017.ecs.PhysicsObject
import northburns.lgj2017.ecs.systems.*


private val PLAYER_ID = "player1"

/**
 * Just hack together some prototype in this class. Refactor once there'PLAYER_ID actually something neat :)
 */
class NorthburnsLinuxJamGame2017 : KotlinApplication() {

    lateinit internal var batch: SpriteBatch
    lateinit internal var box2dDebugRenderer: Box2DDebugRenderer
    lateinit internal var world: World

    val engine = Engine()
    private val camera = OrthographicCamera()
    private val viewport: Viewport = FitViewport(DefaultViewPortSize.x, DefaultViewPortSize.y, camera)

    override fun create() {
        loadAll()

        val multiplexer = InputMultiplexer()
        multiplexer.addProcessor(UiInputProcessor())
        val gameInputProcessor = GameInputProcessor<GameInput>()
        GameInput.register(gameInputProcessor)
        multiplexer.addProcessor(gameInputProcessor)
        Gdx.input.inputProcessor = multiplexer

        batch = SpriteBatch()
        box2dDebugRenderer = Box2DDebugRenderer()

        world = World(Vector2(0f, 0f), true)

        // Create walls
        createWallsForArea(GameAreaSize, world)

        // Create ground image
        Entity().apply {
            add(FixedSprite(Images.ground()))
            add(Positioned(z = -10, width = GameAreaSize.x, height = GameAreaSize.y))
            engine.addEntity(this)
        }

        // Create an Entity
        Entity().apply {
            add(PlayerControlled(PLAYER_ID))
            add(FixedSprite(Images.player()))
            add(PhysicsObject(createUnitBalls(1, world)[0]))
            add(Positioned(z = 1000))
            engine.addEntity(this)
        }

        engine.apply {
            var priority = 0
            // 1 = Input
            addSystem(InputIntentionSystem(gameInputProcessor, ++priority))
            // 2 = React to input
            addSystem(InputProcessingSystem(gameInputProcessor, ++priority))
            addSystem(CameraSystem(camera, ++priority))

            // 3 = Render
            addSystem(RenderingSystem(camera, batch, Box2dDebugRenderer(box2dDebugRenderer, world), ++priority))
            // 4 = Update Box2d world
            addSystem(Box2dUpdateSystem(world, ++priority))
            addSystem(Box2dBodyToPositionedSystem(++priority))

        }


    }

    override fun render(delta: Float) {
        clearScreen(0f, 0.1f, 0f)
        engine.update(delta)
    }

    override fun dispose() {
        world.dispose()
        box2dDebugRenderer.dispose()
        batch.dispose()
        disposeAll()
    }

    override fun resize(width: Int, height: Int) {
        viewport.update(width, height)
    }
}


@file:JvmName("DesktopLauncher")

package northburns.lgj2017.desktop

import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import northburns.lgj2017.NorthburnsLinuxJamGame2017


fun main(vararg args: String) {
    val config = LwjglApplicationConfiguration()
    LwjglApplication(NorthburnsLinuxJamGame2017(), config)
}